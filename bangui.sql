-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 03 juin 2020 à 14:17
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bangui`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `idCategories` int(11) NOT NULL,
  `nomCategories` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `employes`
--

CREATE TABLE `employes` (
  `idEmployes` int(11) NOT NULL,
  `nomEmployes` varchar(50) NOT NULL,
  `prenomEmployes` varchar(50) NOT NULL,
  `sexeEmployes` varchar(50) NOT NULL,
  `adresseEmployes` varchar(50) NOT NULL,
  `telephoneEmployes` varchar(50) NOT NULL,
  `photoEmployes` varchar(50) NOT NULL,
  `matriculeEntreprises` varchar(50) NOT NULL,
  `emailEmployes` varchar(50) NOT NULL,
  `identifientEmployes` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `entreprises`
--

CREATE TABLE `entreprises` (
  `idEntreprises` int(11) NOT NULL,
  `matriculeEntreprises` varchar(50) NOT NULL,
  `raisonSocialEntreprises` varchar(50) NOT NULL,
  `statutJuridiqueEntreprises` varchar(50) NOT NULL,
  `adresseEntreprises` varchar(50) NOT NULL,
  `contactEntreprises` varchar(50) NOT NULL,
  `emailEntreprises` varchar(50) NOT NULL,
  `telephoneEntreprises` varchar(50) NOT NULL,
  `logoEntreprises` varchar(50) NOT NULL,
  `rccmEntreprises` varchar(50) NOT NULL,
  `createparEntreprises` varchar(50) NOT NULL,
  `dateCreationEntreprises` timestamp NOT NULL DEFAULT current_timestamp(),
  `idGestionnaires` int(11) NOT NULL,
  `identifientutilisateurs` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `entreprises`
--

INSERT INTO `entreprises` (`idEntreprises`, `matriculeEntreprises`, `raisonSocialEntreprises`, `statutJuridiqueEntreprises`, `adresseEntreprises`, `contactEntreprises`, `emailEntreprises`, `telephoneEntreprises`, `logoEntreprises`, `rccmEntreprises`, `createparEntreprises`, `dateCreationEntreprises`, `idGestionnaires`, `identifientutilisateurs`) VALUES
(11, 'BANGUI102020GN', 'CEDIG', 'SARL', 'CITE', '6555', 'alseny@gmail.com', '6444', '5ead65d7a470900231k3ytubx53k_ts=1488281498000.jpg', 'RCCM', 'bamba', '2020-05-02 12:21:43', 2, 'cediguinee');

-- --------------------------------------------------------

--
-- Structure de la table `gestionnaires`
--

CREATE TABLE `gestionnaires` (
  `idGestionnaires` int(11) NOT NULL,
  `nomGestionnaires` varchar(50) NOT NULL,
  `prenomGestionnaires` varchar(50) NOT NULL,
  `sexeGestionnaires` varchar(50) NOT NULL,
  `telephoneGestionnaires` varchar(50) NOT NULL,
  `contactGestionnaires` varchar(50) NOT NULL,
  `adresseGestionnaires` varchar(50) NOT NULL,
  `emailGestionnaires` varchar(50) NOT NULL,
  `fonctionGestionnaires` varchar(50) NOT NULL,
  `professionGestionnaires` varchar(50) NOT NULL,
  `identifientutilisateurs` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `gestionnaires`
--

INSERT INTO `gestionnaires` (`idGestionnaires`, `nomGestionnaires`, `prenomGestionnaires`, `sexeGestionnaires`, `telephoneGestionnaires`, `contactGestionnaires`, `adresseGestionnaires`, `emailGestionnaires`, `fonctionGestionnaires`, `professionGestionnaires`, `identifientutilisateurs`) VALUES
(2, 'Loua', 'Simone', 'Masculin', '622222223', '655555553', 'Mamou', 'simone@gmail.com', 'Docteur', 'Docteur', 'cediguinee');

-- --------------------------------------------------------

--
-- Structure de la table `idmatricule`
--

CREATE TABLE `idmatricule` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `idmatricule`
--

INSERT INTO `idmatricule` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11);

-- --------------------------------------------------------

--
-- Structure de la table `proprietaires`
--

CREATE TABLE `proprietaires` (
  `idProprietaires` int(11) NOT NULL,
  `nomProprietaires` varchar(50) NOT NULL,
  `prenomProprietaires` varchar(50) NOT NULL,
  `adresseProprietaires` varchar(50) NOT NULL,
  `telephoneProprietaires` varchar(50) NOT NULL,
  `emailProprietaires` varchar(50) NOT NULL,
  `matriculeEntreprises` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `idUtilisateurs` int(11) NOT NULL,
  `identifientutilisateurs` varchar(50) NOT NULL,
  `motdepasseutilisateurs` varchar(255) NOT NULL,
  `roleutilisateurs` varchar(50) NOT NULL,
  `etatutilisateurs` varchar(1) NOT NULL,
  `matriculeEntreprises` varchar(50) NOT NULL,
  `emailentutilisateurs` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`idUtilisateurs`, `identifientutilisateurs`, `motdepasseutilisateurs`, `roleutilisateurs`, `etatutilisateurs`, `matriculeEntreprises`, `emailentutilisateurs`) VALUES
(3, 'cediguinee', '74ebf3b122a135fd3bcaf14fd2bd3add51aea884a6e0f8da275a4b02d01c388a', 'Administrateur', '1', 'BANGUI102020GN', 'alseny@gmail.com');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`idCategories`);

--
-- Index pour la table `employes`
--
ALTER TABLE `employes`
  ADD PRIMARY KEY (`idEmployes`),
  ADD KEY `identifientEmployes` (`identifientEmployes`);

--
-- Index pour la table `entreprises`
--
ALTER TABLE `entreprises`
  ADD PRIMARY KEY (`idEntreprises`),
  ADD UNIQUE KEY `matriculeEntreprises` (`matriculeEntreprises`),
  ADD KEY `matriculeEntreprises_2` (`matriculeEntreprises`),
  ADD KEY `idGestionnaires` (`idGestionnaires`);

--
-- Index pour la table `gestionnaires`
--
ALTER TABLE `gestionnaires`
  ADD PRIMARY KEY (`idGestionnaires`),
  ADD KEY `identifientutilisateurs` (`identifientutilisateurs`);

--
-- Index pour la table `idmatricule`
--
ALTER TABLE `idmatricule`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `proprietaires`
--
ALTER TABLE `proprietaires`
  ADD PRIMARY KEY (`idProprietaires`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`idUtilisateurs`),
  ADD KEY `matriculeEntreprises` (`matriculeEntreprises`),
  ADD KEY `identifientutilisateurs` (`identifientutilisateurs`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `idCategories` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `employes`
--
ALTER TABLE `employes`
  MODIFY `idEmployes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `entreprises`
--
ALTER TABLE `entreprises`
  MODIFY `idEntreprises` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `gestionnaires`
--
ALTER TABLE `gestionnaires`
  MODIFY `idGestionnaires` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `idmatricule`
--
ALTER TABLE `idmatricule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `proprietaires`
--
ALTER TABLE `proprietaires`
  MODIFY `idProprietaires` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `idUtilisateurs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `entreprises`
--
ALTER TABLE `entreprises`
  ADD CONSTRAINT `entreprises_idgestionnaire_fk` FOREIGN KEY (`idGestionnaires`) REFERENCES `gestionnaires` (`idGestionnaires`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD CONSTRAINT `utilisateur_entreprise_fk` FOREIGN KEY (`matriculeEntreprises`) REFERENCES `entreprises` (`matriculeEntreprises`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
