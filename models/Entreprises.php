<?php


namespace models;


class Entreprises
{
    public static function getCountMatricule():int{
        global $db;
        $req=$db->prepare("SELECT * FROM idmatricule");
        $req->execute();
        $req->closeCursor();
        return $req->rowCount();
    }
    public static function addMatricule():void{
        global $db;
        $req=$db->prepare("INSERT INTO idmatricule (id) VALUES (NULL);");
        $req->execute();
    }
    public static function addEntreprise(string $matriculeEntreprises, string $raisonSocialEntreprises, string $statutJuridiqueEntreprises, string $adresseEntreprises, string $contactEntreprises, string $emailEntreprises, string $telephoneEntreprises, string $logoEntreprises, string $rccmEntreprises, string $createparEntreprises, string $idGestionnaires, string $identifientutilisateur):void{
        global $db;
        $req=$db->prepare("INSERT INTO entreprises (matriculeEntreprises, raisonSocialEntreprises, statutJuridiqueEntreprises, adresseEntreprises, contactEntreprises, emailEntreprises, telephoneEntreprises, logoEntreprises, rccmEntreprises, createparEntreprises, idGestionnaires, identifientutilisateurs) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
        $req->execute([secure($matriculeEntreprises), secure($raisonSocialEntreprises), secure($statutJuridiqueEntreprises), secure($adresseEntreprises), secure($contactEntreprises), secure($emailEntreprises), secure($telephoneEntreprises), secure($logoEntreprises), secure($rccmEntreprises), secure($createparEntreprises), secure($idGestionnaires), secure($identifientutilisateur)]);
        $req->closeCursor();
    }
    public static function getMatriculeEntreprise(string $email):array {
        global $db;
        $req=$db->prepare("SELECT * FROM gestionnaires INNER JOIN entreprises WHERE entreprises.idGestionnaires = gestionnaires.idGestionnaires AND gestionnaires.emailGestionnaires = ?");
        $req->execute([secure($email)]);
        $resultats = [];
        while($data = $req->fetchObject()){
            array_push($resultats,$data);
        }
        $req->closeCursor();
        return $resultats;

    }
}