<?php


namespace models;


class Utilisateurs
{
    public static function addUsers(string $identifientutilisateurs, string $motdepasseutilisateurs, string $roleutilisateurs, string $etatutilisateurs, string $matriculeEntreprises, string $emailentutilisateurs):void {
        global $db;
        $req=$db->prepare("INSERT INTO utilisateurs (identifientutilisateurs, motdepasseutilisateurs, roleutilisateurs, etatutilisateurs, matriculeEntreprises, emailentutilisateurs) VALUES (?, ?, ?, ?, ?, ?);");
        $req->execute([secure($identifientutilisateurs), secure($motdepasseutilisateurs), secure($roleutilisateurs), secure($etatutilisateurs), secure($matriculeEntreprises), secure($emailentutilisateurs)]);
        $req->closeCursor();
    }

    public static function verificationLogin(string $identifientutilisateurs, string $motdepasseutilisateurs):array {
        global $db;
        $req=$db->prepare("SELECT * FROM utilisateurs INNER JOIN entreprises WHERE utilisateurs.matriculeEntreprises = entreprises.matriculeEntreprises AND utilisateurs.identifientutilisateurs = ? AND utilisateurs.motdepasseutilisateurs = ?;");
        $req->execute([secure($identifientutilisateurs), secure($motdepasseutilisateurs)]);
        $resultats = [];
        while($donne = $req->fetchObject()){
            array_push($resultats,$donne);
        }
        $req->closeCursor();
        return $resultats;
    }
    public static function getUsersByIdentifient(string $identifientutilisateurs):array {
        global $db;
        $req=$db->prepare("SELECT * FROM utilisateurs INNER JOIN gestionnaires INNER JOIN entreprises WHERE entreprises.idGestionnaires = gestionnaires.idGestionnaires AND gestionnaires.identifientutilisateurs=utilisateurs.identifientutilisateurs AND utilisateurs.identifientutilisateurs = ?;");
        $req->execute([secure($identifientutilisateurs)]);
        $resultats = [];
        while($donne = $req->fetchObject()){
            array_push($resultats,$donne);
        }
        $req->closeCursor();
        return $resultats;
    }
}