<?php


namespace models;


class Gestionnaires
{
    public static function addGestionnaire(string $nomGestionnaires, string $prenomGestionnaires, string $sexeGestionnaires, string $telephoneGestionnaires, string $contactGestionnaires, string $adresseGestionnaires, string $emailGestionnaires, string $fonctionGestionnaires, string $professionGestionnaires, string $identifientutilisateurs):void{
        global $db;
        $req=$db->prepare("INSERT INTO gestionnaires (nomGestionnaires, prenomGestionnaires, sexeGestionnaires, telephoneGestionnaires, contactGestionnaires, adresseGestionnaires, emailGestionnaires, fonctionGestionnaires, professionGestionnaires, identifientutilisateurs) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
        $req->execute([secure($nomGestionnaires), secure($prenomGestionnaires), secure($sexeGestionnaires), secure($telephoneGestionnaires), secure($contactGestionnaires), secure($adresseGestionnaires) , secure($emailGestionnaires), secure($fonctionGestionnaires), secure($professionGestionnaires), secure($identifientutilisateurs)]);
        $req->closeCursor();
    }
    public static function editGestionnaire(string $nomGestionnaires, string $prenomGestionnaires, string $sexeGestionnaires, string $telephoneGestionnaires, string $contactGestionnaires, string $adresseGestionnaires, string $emailGestionnaires, string $fonctionGestionnaires, string $professionGestionnaires, string $identifientutilisateurs,string $id):void{
        global $db;
        $req=$db->prepare("UPDATE gestionnaires SET nomGestionnaires = ?, prenomGestionnaires = ?, sexeGestionnaires = ?, telephoneGestionnaires = ?, contactGestionnaires = ?, adresseGestionnaires = ?, emailGestionnaires = ?, fonctionGestionnaires = ?, professionGestionnaires = ?, identifientutilisateurs = ? WHERE idGestionnaires = ?;");
        $req->execute([secure($nomGestionnaires), secure($prenomGestionnaires), secure($sexeGestionnaires), secure($telephoneGestionnaires), secure($contactGestionnaires), secure($adresseGestionnaires) , secure($emailGestionnaires), secure($fonctionGestionnaires), secure($professionGestionnaires), secure($identifientutilisateurs), secure($id)]);
        $req->closeCursor();
    }
    public static function getGestionnaireById(string $id):array {
        global $db;
        $req=$db->prepare("SELECT * FROM gestionnaires WHERE idGestionnaires = ?;");
        $req->execute([secure($id)]);
        $resultats = [];
        while($data =$req->fetchObject()):
            array_push($resultats,$data);
        endwhile;
        $req->closeCursor();
        return $resultats;

    }
    public static function getAllGestionnaires():array {
        global $db;
        $req=$db->prepare("SELECT * FROM gestionnaires ORDER BY idGestionnaires DESC;");
        $req->execute();
        $resultats = [];
        while($data =$req->fetchObject()):
            array_push($resultats,$data);
        endwhile;
        $req->closeCursor();
        return $resultats;

    }
    public static function deleteGestionnaire(string $idGestionnaire):void{
        global $db;
        $req=$db->prepare("DELETE FROM gestionnaires WHERE idGestionnaires = ?");
        $req->execute([secure($idGestionnaire)]);
        $req->closeCursor();
    }
}