<?php


namespace models;


class Employes
{
    public static function addEmployes(string $nomEmployes, string $prenomEmployes, string $sexeEmployes, string $adresseEmployes, string $telephoneEmployes, string $photoEmployes, string $matriculeEntreprises, string $emailEmployes, string $identifientEmployes):void{
        global $db;
        $req=$db->prepare("INSERT INTO employes (nomEmployes, prenomEmployes, sexeEmployes, adresseEmployes, telephoneEmployes, photoEmployes, matriculeEntreprises, emailEmployes, identifientEmployes) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);");
        $req->execute([secure($nomEmployes), secure($prenomEmployes), secure($sexeEmployes), secure($adresseEmployes), secure($telephoneEmployes), secure($photoEmployes) , secure($matriculeEntreprises), secure($emailEmployes), secure($identifientEmployes)]);
        $req->closeCursor();
    }
    public static function editEmployes(string $nomEmployes, string $prenomEmployes, string $sexeEmployes, string $adresseEmployes, string $telephoneEmployes, string $photoEmployes, string $matriculeEntreprises, string $emailEmployes, string $identifientEmployes,string $id):void{
        global $db;
        $req=$db->prepare("UPDATE employes SET nomEmployes = ?, prenomEmployes = ?, sexeEmployes = ?, adresseEmployes = ?, telephoneEmployes = ?, photoEmployes = ?, matriculeEntreprises = ?, emailEmployes = ?, identifientEmployes = ? WHERE idEmployes = ?;");
        $req->execute([secure($nomEmployes), secure($prenomEmployes), secure($sexeEmployes), secure($adresseEmployes), secure($telephoneEmployes), secure($photoEmployes) , secure($matriculeEntreprises), secure($emailEmployes), secure($identifientEmployes), secure($id)]);
        $req->closeCursor();
    }
    public static function getEmployesById(string $id):array {
        global $db;
        $req=$db->prepare("SELECT * FROM employes WHERE idEmployes = ?;");
        $req->execute([secure($id)]);
        $resultats = [];
        while($data =$req->fetchObject()):
            array_push($resultats,$data);
        endwhile;
        $req->closeCursor();
        return $resultats;

    }
    public static function getAllEmployes():array {
        global $db;
        $req=$db->prepare("SELECT * FROM employes ORDER BY idEmployes DESC;");
        $req->execute();
        $resultats = [];
        while($data =$req->fetchObject()):
            array_push($resultats,$data);
        endwhile;
        $req->closeCursor();
        return $resultats;

    }
    public static function deleteEmployes(string $idEmployes):void{
        global $db;
        $req=$db->prepare("DELETE FROM employes WHERE $idEmployes = ?");
        $req->execute([secure($idEmployes)]);
        $req->closeCursor();
    }
}