<?php

require_once ('configs/config.php');
require_once ('fonctions/fonctions.php');
require_once ('configs/db.php');
require_once ('autoloader/Autoloader.php');
use autoloader\Autoloader;

Autoloader::register();
if(isset($_GET['page']) AND !empty($_GET['page'])){
    $page=secure($_GET['page']);
}else{
    $page="accueil";
}

$allPages=scandir("controllers/");
if(in_array($page.'_controller.php',$allPages)){

    if($page=='acceuil' OR $page=='tableau_de_bord'){

    }

    require_once ("controllers/{$page}_controller.php");
    require_once ("views/{$page}_view.php");
}else{
    $page="erreur";
    require_once ("views/{$page}_view.php");
}