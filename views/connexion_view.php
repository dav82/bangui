<!DOCTYPE html>
<html lang="en">
    <?php require_once ("includes/head.php");?>
<body>
<div id="main">
    <?php require_once ("includes/navbar.php");?>
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col col-md-12 col-lg-10 col-xl-8">
                <div class="page-header">
                    <h1>Connexion</h1>
                </div>
            </div>
        </div>
    </div>
    <div id="content">
        <div class="container">
            <div class="row justify-content-md-center align-items-center">
                <div class="col col-md-7 p-0  col-lg-8 col-xl-9 ">
                    <?php if(isset($success) AND !empty($success)):?>
                        <?php foreach ($success as $get):?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <i class="fa fa-check-circle"> Information : <?=$get?></i>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                    <?php if(isset($warnings) AND !empty($warnings)):?>
                        <?php foreach ($warnings as $get):?>
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <i class="fa fa-info-circle"> Information : </i>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                    <?php if(isset($errors) AND !empty($errors)):?>
                        <?php foreach ($errors as $get):?>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <i class="fa fa-close"> Information : </i>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php endforeach;?>
                    <?php endif;?>
                </div>
                <div class="col col-md-6  col-lg-5 col-xl-4">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="login">
                            <form action="" method="post">
                                <div class="form-group">
                                    <label for="name">Identifiant</label>
                                    <?php global $identifientutilisateurs?>
                                    <input type="text" name="identifientutilisateurs"  id="name" value="<?=$identifientutilisateurs?>" class="form-control form-control-lg" placeholder="Votre Identifiant">
                                </div>
                                <div class="form-group">
                                    <label for="password">Mot de Passe</label>
                                    <?php global $motdepasseutilisateurs?>
                                    <input type="password" name="motdepasseutilisateurs" id="password" value="<?=$motdepasseutilisateurs?>" class="form-control form-control-lg" placeholder="Votre Mot de Passe">
                                </div>
                                <p class="text-lg-right"><a href="#">Mot de Passe Oublier</a></p>
                                <div class="checkbox">
                                    <input type="checkbox" id="remember_me">
                                    <label for="remember_me">Se souvenir de moi</label>
                                </div>
                                <input type="submit" class="btn btn-primary" name="Connexion" value="Connectez-vous">
                                <input type="reset" class="btn btn-dark" name="Annuler" value="Annuler">
                            </form>
                        </div>
                    </div>
                    <div> </div>
                </div>

                <div class="col-md-6 col-lg-5 col-xl-4">
                    <div class="socal-login-buttons"> <a href="#" class="btn btn-social btn-block btn-facebook"><i class="icon fa fa-facebook"></i> Continue with Facebook</a> <a href="#" class="btn btn-social btn-block btn-twitter"><i class="icon fa fa-twitter"></i> Continue with Twitter</a> <a href="#" class="btn btn-social btn-block btn-google"><i class="icon fa fa-google"></i> Continue with Google</a> </div>
                </div>
            </div>
        </div>
    </div>
    <button class="btn btn-primary btn-circle" id="to-top"><i class="fa fa-angle-up"></i></button>
    <?php require_once ("includes/footer.php");?>
</div>

</body></html>