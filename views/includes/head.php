<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Real Estate</title>

    <!-- Bootstrap -->
    <link href="https://fonts.googleapis.com/css?family=Libre+Franklin:100,200,300,400,500,700" rel="stylesheet">
    <link href="<?=LINK?>assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=LINK?>assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?=LINK?>assets/lib/animate.css" rel="stylesheet">
    <link href="<?=LINK?>assets/lib/selectric/selectric.css" rel="stylesheet">
    <link href="<?=LINK?>assets/lib/swiper/css/swiper.min.css" rel="stylesheet">
    <link href="<?=LINK?>assets/lib/aos/aos.css" rel="stylesheet">
    <link href="<?=LINK?>assets/lib/Magnific-Popup/magnific-popup.css" rel="stylesheet">
    <link href="<?=LINK?>assets/css/style.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=LINK?>assets/lib/jquery-3.2.1.min.js"></script>
    <script src="<?=LINK?>assets/lib/popper.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=LINK?>assets/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?=LINK?>assets/lib/selectric/jquery.selectric.js"></script>
    <script src="<?=LINK?>assets/lib/swiper/js/swiper.min.js"></script>
    <script src="<?=LINK?>assets/lib/aos/aos.js"></script>
    <script src="<?=LINK?>assets/lib/Magnific-Popup/jquery.magnific-popup.min.js"></script>
    <script src="<?=LINK?>assets/lib/sticky-sidebar/ResizeSensor.min.js"></script>
    <script src="<?=LINK?>assets/lib/sticky-sidebar/theia-sticky-sidebar.min.js"></script>
    <script src="<?=LINK?>assets/lib/lib.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
