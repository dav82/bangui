<div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static; transform: none; top: 0px; left: 331.5px;">
    <div id="sidebar" class="sidebar-left">
        <div class="sidebar_inner">
            <div class="list-group no-border list-unstyled">
                <span class="list-group-item heading">Espace d'Administration</span>
                <a href="<?=LINK.'ajout_gestionnaire'?>" class="list-group-item active">
                    <i class="fa fa-fw fa-plus-square-o"></i> Ajout D'un Gestionnaire</a>
                <a href="<?=LINK.'liste_des_gestionnaires'?>" class="list-group-item d-flex justify-content-between align-items-center">
                    <span><i class="fa fa-fw fa-bookmark-o"></i> Liste des Gestionnaires</span>
                    <span class="badge badge-primary badge-pill">10</span>
                </a>
                <a href="<?=LINK.'ajout_employes'?>" class="list-group-item d-flex justify-content-between align-items-center">
                    <span><i class="fa fa-fw fa-bookmark-o"></i> Ajout Employé</span>
                    <span class="badge badge-primary badge-pill">10</span>
                </a>
                <a href="<?=LINK.'liste_des_employes'?>" class="list-group-item d-flex justify-content-between align-items-center">
                    <span><i class="fa fa-fw fa-bookmark-o"></i> Liste des Employés</span>
                    <span class="badge badge-primary badge-pill">10</span>
                </a>
                <a href="<?=LINK.'ajout_entreprise'?>" class="list-group-item d-flex justify-content-between align-items-center"><span><i class="fa fa-fw fa-bars"></i> Ajout d'une Entreprise</span>
                    <span class="badge badge-primary badge-pill">7</span>
                </a>
                <span class="list-group-item heading">Manage Account</span>
                <a href="my_profile.html" class="list-group-item"><i class="fa fa-fw fa-pencil"></i> My Profile</a>
                <a href="my_password.html" class="list-group-item"><i class="fa fa-fw fa-lock"></i> Change Password</a>
                <a href="my_notifications.html" class="list-group-item"><i class="fa fa-fw fa-bell-o"></i> Notifications</a>
                <a href="my_membership.html" class="list-group-item"><i class="fa fa-fw fa-cubes"></i> Membership</a>
                <a href="my_payments.html" class="list-group-item"><i class="fa fa-fw fa-credit-card"></i> Payments</a>
                <a href="my_account.html" class="list-group-item"><i class="fa fa-fw fa-cog"></i> Account</a>
            </div>
        </div>
    </div>
    <div class="resize-sensor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; z-index: -1; visibility: hidden;">
        <div class="resize-sensor-expand" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;">
            <div style="position: absolute; left: 0px; top: 0px; transition: all 0s ease 0s; width: 550px; height: 567px;">

            </div>
        </div>
        <div class="resize-sensor-shrink" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;">
            <div style="position: absolute; left: 0; top: 0; transition: 0s; width: 200%; height: 200%">

            </div>
        </div>
    </div>
</div>
</div>
