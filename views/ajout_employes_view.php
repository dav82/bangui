<!DOCTYPE html>
<html lang="en">
<?php require_once ("includes/head.php");?>
<body>
<div id="main">
    <?php require_once ("includes/navbar.php");?>
    <div id="content" style="transform: none;">
        <div class="container" style="transform: none;">
            <div class="row justify-content-md-center" style="transform: none;">
                <div class="col col-lg-12 col-xl-10" style="transform: none;">
                    <div class="row has-sidebar" style="transform: none;">
                        <div class="col-md-5 col-lg-4 col-xl-4 col-sidebar" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
                            <?php require_once("includes/slider.php"); ?>
                            <div class="col-md-7 col-lg-8 col-xl-8" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
                                <?php if(isset($success) AND !empty($success)):?>
                                    <?php foreach ($success as $get):?>
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            <i class="fa fa-check-circle"> Information : <?=$get?></i>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php endforeach;?>
                                <?php endif;?>
                                <?php if(isset($warnings) AND !empty($warnings)):?>
                                    <?php foreach ($warnings as $get):?>
                                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                            <i class="fa fa-info-circle"> Information : </i>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php endforeach;?>
                                <?php endif;?>
                                <?php if(isset($errors) AND !empty($errors)):?>
                                    <?php foreach ($errors as $get):?>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                          <i class="fa fa-close"> Information : </i>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php endforeach;?>
                                <?php endif;?>
                            <div class="theiaStickySidebar" style="padding-top: 1px; padding-bottom: 1px; position: static; transform: none;"><div class="page-header bordered">
                                    <h1>Nouvel Employé <small>Veuillez saisir correctement les informations de l'employé</small></h1>
                                </div><form action="" method="post">
                                    <div class="form-group">
                                        <label for="title">Matricule Entreprise</label>
                                        <input type="text" class="form-control form-control-lg" readonly name="matricule" value="<?=(!$m=='')?$m:''?>" id="title" placeholder="Veuillez saisir le prénom" autofocus="">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Identifiant Employé</label>
                                        <input type="text" class="form-control form-control-lg" name="identifient" id="title" placeholder="Veuillez saisir l'identifiant" autofocus="">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Prénom</label>
                                        <input type="text" class="form-control form-control-lg" name="prenom" id="title" placeholder="Veuillez saisir le prénom" autofocus="">
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Nom</label>
                                        <input type="text" class="form-control form-control-lg" name="nom" id="title" placeholder="Veuillez saisir le nom" autofocus="">
                                    </div>
                                    <div class="form-group">
                                        <label>Sexe</label>
                                        <div>
                                            <div class="radio radio-inline">
                                                <input type="radio" name="sexe" id="rent" value="Masculin">
                                                <label for="rent">Masculin</label>
                                            </div>
                                            <div class="radio radio-inline">
                                                <input type="radio" name="sexe" id="sale" value="Feminin">
                                                <label for="sale">Féminin</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Adresse</label>
                                        <input type="text" class="form-control form-control-lg" name="adresse" placeholder="Veuillez saisir l'adresse">
                                    </div>
                                    <div class="form-group">
                                        <label>Téléphone</label>
                                        <input type="text" class="form-control form-control-lg" name="telephone" placeholder="Veuillez saisir le numéro">
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" class="form-control form-control-lg" name="email" placeholder="Veuillez saisir l'email">
                                    </div>
                                    <div class="row">
                                    <div class="col-sm-12">
                                        <label>Photo</label>
                                        <input type="file" class="form-control form-control-lg" name="photo" placeholder="Veuillez séléctionner la photo">
                                    </div>
                                    </div>
                                    </hr>
                                    <input type="submit" class="btn btn-outline-dark" value="Enregistrer">
                                    <input type="reset" class="btn btn-outline-warning" value="Annuler">
                                </form>
                                <div class="resize-sensor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; z-index: -1; visibility: hidden;"><div class="resize-sensor-expand" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;"><div style="position: absolute; left: 0px; top: 0px; transition: all 0s ease 0s; width: 550px; height: 2541px;"></div></div><div class="resize-sensor-shrink" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;"><div style="position: absolute; left: 0; top: 0; transition: 0s; width: 200%; height: 200%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php require_once ("includes/footer.php");?>
</div>
</body>
</html>
