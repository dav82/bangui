<!DOCTYPE html>
<html lang="en">
<?php require_once ("includes/head.php");?>
<body>
<div id="main">
    <?php require_once ("includes/navbar.php");?>
    <div id="content" style="transform: none;">
        <div class="container" style="transform: none;">
            <div class="row justify-content-md-center" style="transform: none;">
                <div class="col col-lg-12 col-xl-10" style="transform: none;">
                    <div class="row has-sidebar" style="transform: none;">
                        <div class="col-md-5 col-lg-4 col-xl-4 col-sidebar" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">

                            <?php require_once("includes/slider.php"); ?>
                            <div class="col-md-8 col-lg-8 col-xl-8" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
                                <?php if(isset($success) AND !empty($success)):?>
                                    <?php foreach ($success as $get):?>
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            <i class="fa fa-check-circle"> Information : <?=$get?></i>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php endforeach;?>
                                <?php endif;?>
                                <?php if(isset($warnings) AND !empty($warnings)):?>
                                    <?php foreach ($warnings as $get):?>
                                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                            <i class="fa fa-info-circle"> Information : </i>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php endforeach;?>
                                <?php endif;?>
                                <?php if(isset($errors) AND !empty($errors)):?>
                                    <?php foreach ($errors as $get):?>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <i class="fa fa-close"> Information : </i>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php endforeach;?>
                                <?php endif;?>
                                <div class="theiaStickySidebar" style="padding-top: 1px; padding-bottom: 1px; position: static; transform: none;"><div class="page-header bordered">
                                        <h1>Liste des Gestionnaire <small>Ceci est la liste détaillée des gestionnaires</small></h1>
                                    </div>
                                        <table class="table table-responsive">
                                            <tr>
                                                <th>Prénoms</th>
                                                <th>Nom</th>
                                                <th>Téléphone</th>
                                                <th>Adresses</th>
                                                <th>Email</th>
                                                <th>Fonctions</th>
                                                <th>Professions</th>
                                                <th>Actions</th>
                                            </tr>
                                            <tbody>
                                            <?php if(isset($getAllGestionnaires) AND !empty($getAllGestionnaires)):?>
                                            <?php foreach ($getAllGestionnaires as $getGestionnaire):?>
                                                <tr>
                                                    <td><?=ucfirst($getGestionnaire->prenomGestionnaires)?></td>
                                                    <td><?=strtoupper($getGestionnaire->nomGestionnaires)?></td>
                                                    <td><?=number_format($getGestionnaire->telephoneGestionnaires,'0',' ','-')?></td>
                                                    <td><?=ucfirst($getGestionnaire->adresseGestionnaires)?></td>
                                                    <td><?=strtolower($getGestionnaire->emailGestionnaires)?></td>
                                                    <td><?=ucfirst($getGestionnaire->fonctionGestionnaires)?></td>
                                                    <td><?=ucfirst($getGestionnaire->professionGestionnaires)?></td>
                                                    <td>
                                                        <a href="<?=LINK."liste_des_gestionnaires/".$getGestionnaire->idGestionnaires?>"><i class="fa fa-trash"></i></a>
                                                        <a href="<?=LINK."modification_gestionnaire/".$getGestionnaire->idGestionnaires?>"><i class="fa fa-edit"></i></a>
                                                    </td>
                                                </tr>
                                            <?php endforeach;?>
                                            <?php endif;?>
                                            </tbody>
                                        </table>
                                    <div class="resize-sensor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; z-index: -1; visibility: hidden;"><div class="resize-sensor-expand" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;"><div style="position: absolute; left: 0px; top: 0px; transition: all 0s ease 0s; width: 550px; height: 2541px;"></div></div><div class="resize-sensor-shrink" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;"><div style="position: absolute; left: 0; top: 0; transition: 0s; width: 200%; height: 200%">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php require_once ("includes/footer.php");?>
    </div>
</body>
</html>
