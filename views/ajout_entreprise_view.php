<!DOCTYPE html>
<html lang="en">
<?php require_once ("includes/head.php");?>
<body>
<div id="main">
    <?php require_once ("includes/navbar.php");?>
    <div id="content" style="transform: none;">
        <div class="container" style="transform: none;">
            <div class="row justify-content-md-center" style="transform: none;">
                <div class="col col-lg-12 col-xl-10" style="transform: none;">
                    <div class="row has-sidebar" style="transform: none;">
                        <div class="col-md-5 col-lg-4 col-xl-4 col-sidebar" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">

                            <?php require_once("includes/slider.php"); ?>
                            <div class="col-md-7 col-lg-8 col-xl-8" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
                                <?php if(isset($success) AND !empty($success)):?>
                                    <?php foreach ($success as $get):?>
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            <i class="fa fa-check-circle"> Information : <?=$get?></i>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php endforeach;?>
                                <?php endif;?>
                                <?php if(isset($warnings) AND !empty($warnings)):?>
                                    <?php foreach ($warnings as $get):?>
                                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                            <i class="fa fa-info-circle"> Information : </i>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php endforeach;?>
                                <?php endif;?>
                                <?php if(isset($errors) AND !empty($errors)):?>
                                    <?php foreach ($errors as $get):?>
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <i class="fa fa-close"> Information : </i>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php endforeach;?>
                                <?php endif;?>
                                <div class="theiaStickySidebar" style="padding-top: 1px; padding-bottom: 1px; position: static; transform: none;"><div class="page-header bordered">
                                        <h1>Nouvelle Entreprise<small>Veuillez saisir correctement les informations de l'Entreprise</small></h1>
                                    </div><form action="" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label for="title">Matricule</label>
                                            <input type="text" class="form-control form-control-lg" readonly name="matriculeentreprise" value="<?=NAME_ENTREPRISE.\models\Entreprises::getCountMatricule().date('Y')."GN"?>" id="title" placeholder="Veuillez saisir le prénom" autofocus="">
                                        </div>
                                        <div class="form-group">
                                            <label for="title">Raison Social</label>
                                            <input type="text" class="form-control form-control-lg" name="nomentreprise" id="title" placeholder="Veuillez saisir la raison social" autofocus="">
                                        </div>
                                        <div class="form-group">
                                            <label for="title">Statut Juridique</label>
                                            <input type="text" class="form-control form-control-lg" name="statusjuridiqueentreprise" id="title" placeholder="Veuillez saisir le statut juridique" autofocus="">
                                        </div>
                                        <div class="form-group">
                                            <label for="title">Adresse</label>
                                            <input type="text" class="form-control form-control-lg" name="adresseentreprise" id="title" placeholder="Veuillez saisir l'adresse" autofocus="">
                                        </div>
                                        <div class="form-group">
                                            <label>Téléphone 1</label>
                                            <input type="text" class="form-control form-control-lg" name="telephone1" placeholder="Veuillez saisir le téléphone 1">
                                        </div>
                                        <div class="form-group">
                                            <label>Téléphone 2</label>
                                            <input type="text" class="form-control form-control-lg" name="telephone2" placeholder="Veuillez saisir le téléphone 1">
                                        </div><div class="form-group">
                                            <label>Identifient</label>
                                            <input type="text" class="form-control form-control-lg" name="identifientutilisateur" placeholder="Veuillez saisir l'identifient">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control form-control-lg" name="email" placeholder="Veuillez saisir l'email">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>RCCM</label>
                                                    <input type="text" class="form-control form-control-lg" name="rccm" placeholder="Veuillez saisir le RCCM">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="profession">Gestionnaire</label>
                                                    <select name="idGestionnaire" class="form-control form-control-lg select" id="">
                                                        <option value="">Veuillez séléctionner un gestionnaire</option>
                                                        <?php if(isset($getGestionnaire) AND !empty($getGestionnaire)):?>
                                                            <?php foreach ($getGestionnaire as $get):?>
                                                                <option value="<?=$get->idGestionnaires?>"><?=$get->prenomGestionnaires." ".$get->nomGestionnaires?></option>
                                                            <?php endforeach;?>
                                                        <?php endif;?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <label>Logo</label>
                                                <input type="file" class="form-control form-control-lg" name="logo" placeholder="Veuillez séléctionner le logo">
                                            </div>
                                        </div>
                                        <br>
                                        </hr>
                                        <input type="submit" class="btn btn-outline-dark" value="Enregistrer">
                                        <input type="reset" class="btn btn-outline-warning" value="Annuler">
                                    </form>
                                    <hr>
                                    <div class="resize-sensor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; z-index: -1; visibility: hidden;"><div class="resize-sensor-expand" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;"><div style="position: absolute; left: 0px; top: 0px; transition: all 0s ease 0s; width: 550px; height: 2541px;"></div></div><div class="resize-sensor-shrink" style="position: absolute; left: 0; top: 0; right: 0; bottom: 0; overflow: hidden; z-index: -1; visibility: hidden;"><div style="position: absolute; left: 0; top: 0; transition: 0s; width: 200%; height: 200%">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php require_once ("includes/footer.php");?>
    </div>
</body>
</html>
