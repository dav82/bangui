<?php

//gestion des message d'erreur
$success = [];
$warnings = [];
$errors = [];
use models\Utilisateurs;
if(isset($_POST) AND !empty($_POST) AND $_POST['Connexion']=="Connectez-vous"){
//Extration du formulaire
    extract($_POST);
    //Verification de la saisi dans le login
    if(isset($identifientutilisateurs) AND empty($identifientutilisateurs)){
        array_push($warnings,"Veuillez saisir votre identifiant");
    }
    //Verification de la saisi dans le password
    if(isset($motdepasseutilisateurs) AND empty($motdepasseutilisateurs)){
        array_push($warnings,"Veuillez saisir votre mot de passe");
    }
    if(count($warnings)==0 AND count($errors)==0){
        //Gestion des exceptions
        debug($_POST);
        try {
            $motdepasse = hashpassword($motdepasseutilisateurs);
            $connexion = Utilisateurs::verificationLogin($identifientutilisateurs,$motdepasse);
            debug($connexion);
            if(count($connexion)==1){
                foreach ($connexion as $get){
                    if($get->etatutilisateurs==1){
                        $_SESSION['bangui']['identifiant'] = $get->identifientutilisateurs;
                        $_SESSION['bangui']['emailUsers'] = $get->emailentutilisateurs;
                        $_SESSION['bangui']['roleUsers'] = $get->roleutilisateurs;
                        $_SESSION['bangui']['etatUsers'] = $get->etatutilisateurs;
                        $_SESSION['bangui']['idUsers'] = $get->idUtilisateurs;
                        header('location:'.LINK.'tableau_de_bord');
                    }else{
                        array_push($warnings,"Veuillez contacter administrateur");
                    }

                }
            }else{
                array_push($warnings,"Erreur de connexion votre login ou mot de passe est incorrect");
            }
        }catch (PDOException $e){
            array_push($warnings,"Erreur de connexion");
        }
    }
    //header('location:index.php');
}