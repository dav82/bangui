<?php
$success = [];
$warnings = [];
$errors = [];
use models\Gestionnaires;
if(isset($_POST) AND !empty($_POST)){
    extract($_POST);
    if(isset($prenom) AND empty($prenom)){
        array_push($warnings,"Veuillez saisir le prénom");
    }if(isset($nom) AND empty($nom)){
        array_push($warnings,"Veuillez saisir le nom");
    }if(isset($sexe) AND empty($sexe)){
        array_push($warnings,"Veuillez saisir le sexe");
    }if(isset($telephone1) AND empty($telephone1)){
        array_push($warnings,"Veuillez saisir le numéro de téléphone 1");
    }if(isset($telephone2) AND empty($telephone2)){
        array_push($warnings,"Veuillez saisir le numéro de téléphone 2");
    }if(isset($email) AND empty($email)){
        array_push($warnings,"Veuillez saisir l'email");
    }if(isset($adresse) AND empty($adresse)){
        array_push($warnings,"Veuillez saisir l'adresse");
    }if(isset($fonction) AND empty($fonction)){
        array_push($warnings,"Veuillez saisir la fonction");
    }if(isset($profession) AND empty($profession)){
        array_push($warnings,"Veuillez saisir la profession");
    }
    if(count($warnings)==0 AND count($errors)==0){
        try {
            Gestionnaires::addGestionnaire($nom,$prenom,$sexe,$telephone1,$telephone2,$adresse,$email,$fonction,$profession,$_SESSION['bangui']['login']);
            array_push($success,"Gestionnaire : {$prenom} {$nom}, crée avec succes");
        }catch (PDOException $e){
            array_push($errors,"Erreur de creation du Gestionnaire : {$prenom} {$nom}");
        }

    }
}

