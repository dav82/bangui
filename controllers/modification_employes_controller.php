<?php

$success = [];
$warnings = [];
$errors = [];

use models\Employes;

if (isset($_POST) and !empty($_POST)) {
    extract($_POST);
    if (isset($id) and empty($id)) {
        array_push($warnings, "Veuillez retourne sur la liste");
    }
    if (isset($prenom) and empty($prenom)) {
        array_push($warnings, "Veuillez saisir le prénom");
    }
    if (isset($nom) and empty($nom)) {
        array_push($warnings, "Veuillez saisir le nom");
    }
    if (isset($sexe) and empty($sexe)) {
        array_push($warnings, "Veuillez saisir le sexe");
    }
    if (isset($telephone1) and empty($telephone1)) {
        array_push($warnings, "Veuillez saisir le numéro de téléphone 1");
    }
    if (isset($telephone2) and empty($telephone2)) {
        array_push($warnings, "Veuillez saisir le numéro de téléphone 2");
    }
    if (isset($email) and empty($email)) {
        array_push($warnings, "Veuillez saisir l'email");
    }
    if (isset($adresse) and empty($adresse)) {
        array_push($warnings, "Veuillez saisir l'adresse");
    }
    if (isset($fonction) and empty($fonction)) {
        array_push($warnings, "Veuillez saisir la fonction");
    }
    if (isset($profession) and empty($profession)) {
        array_push($warnings, "Veuillez saisir la profession");
    }
    if (count($warnings) == 0 and count($errors) == 0) {
        try {
            Employes::editEmployes($nom, $prenom, $sexe, $adresse, $telephone, $matricule, $email, $identifient,$id);
            array_push($success, "Employé : {$prenom} {$nom}, modifié avec succes");} catch (PDOException $e) {
            array_push($errors, "Erreur de modification de l'employé : {$prenom} {$nom}");
        }

    }
}


if(isset($_GET) AND !empty($_GET)){
    extract($_GET);
    if(isset($id) AND !empty($id)){
        $getEmployes = Employes::getEmployesById($id);
    }
}
