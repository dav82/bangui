<?php

$success = [];
$warnings = [];
$errors = [];
use models\Gestionnaires;
if(isset($_GET) AND !empty($_GET)){
    extract($_GET);
    if(isset($id) AND !empty($id)){
        try {
            Gestionnaires::deleteGestionnaire($id);
            array_push($success,"Gestionnaire supprimer avec succès");
        }catch (PDOException $e){
            array_push($errors,"Erreur de suppression");
        }
    }
}
$getAllGestionnaires = Gestionnaires::getAllGestionnaires();

