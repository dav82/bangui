<?php
$success = [];
$warnings = [];
$errors = [];
use models\Entreprises;
$getMatricule = Entreprises::getMatriculeEntreprise($_SESSION['bangui']['emailUsers']);
foreach ($getMatricule as $matricule){
    $m = $matricule->matriculeEntreprises;
}
use models\Employes;
if(isset($_POST) AND !empty($_POST)){
    extract($_POST);
    if(isset($prenom) AND empty($prenom)){
        array_push($warnings,"Veuillez saisir le prénom");
    }if(isset($nom) AND empty($nom)){
        array_push($warnings,"Veuillez saisir le nom");
    }if(isset($sexe) AND empty($sexe)){
        array_push($warnings,"Veuillez saisir le sexe");
    }if(isset($telephone) AND empty($telephone)){
        array_push($warnings,"Veuillez saisir le numéro de téléphone");
    }if(isset($email) AND empty($email)){
        array_push($warnings,"Veuillez saisir l'email");
    }if(isset($adresse) AND empty($adresse)){
        array_push($warnings,"Veuillez saisir l'adresse");
    }if(isset($photo) AND empty($photo)){
        array_push($warnings,"Veuillez choisir la photo");
    }if(isset($identifient) AND empty($identifient)){
        array_push($warnings,"Veuillez saisir l'identifiant");
    }
    if(count($warnings)==0 AND count($errors)==0){
        try {
            Employes::addEmployes($nom, $prenom, $sexe, $adresse, $telephone, $photo, $matricule, $email, $identifient);
            array_push($success,"Employé : {$prenom} {$nom}, crée avec succes");
        }catch (PDOException $e){
            array_push($errors,"Erreur de creation de l'employé : {$prenom} {$nom}");
        }

    }
}

