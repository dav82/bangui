<?php
$success = [];
$warnings = [];
$errors = [];
use models\Gestionnaires;
use models\Entreprises;
use models\Utilisateurs;
if(isset($_POST) AND !empty($_POST)){
    extract($_POST);
    if(isset($matriculeentreprise) AND empty($matriculeentreprise)){
        array_push($warnings,"Veuillez saisir le matricule");
    }if(isset($identifientutilisateur) AND empty($identifientutilisateur)){
        array_push($warnings,"Veuillez saisir l'identifient");
    }
    if(isset($nomentreprise) AND empty($nomentreprise)){
        array_push($warnings,"Veuillez saisir le nom de l'entreprise");
    }
    if(isset($statusjuridiqueentreprise) AND empty($statusjuridiqueentreprise)){
        array_push($warnings,"Veuillez saisir le statut juridique entreprise");
    }
    if(isset($adresseentreprise) AND empty($adresseentreprise)){
        array_push($warnings,"Veuillez saisir l'adresse");
    }
    if(isset($telephone1) AND empty($telephone1)){
        array_push($warnings,"Veuillez saisir le numéro de l'entreprise");
    }
    if(isset($telephone2) AND empty($telephone2)){
        array_push($warnings,"Veuillez saisir le seconde numéro de l'entreprise");
    }
    if(isset($email) AND empty($email)){
        array_push($warnings,"Veuillez saisir l'email de l'entreprise");
    }
    if(isset($rccm) AND empty($rccm)){
        array_push($warnings,"Veuillez saisir le rccm");
    }
    if(isset($idGestionnaire) AND empty($idGestionnaire)){
        array_push($warnings,"Veuillez séléctionner le gestionnaire");
    }
    if(isset($_FILES) AND !empty($_FILES)){
        extract($_FILES);
        $namephoto = $_FILES["logo"]["name"];
        $typephoto = pathinfo($namephoto, PATHINFO_EXTENSION);;
        $filetmp = $_FILES["logo"]["tmp_name"];
        $errorfile = $_FILES["logo"]["error"];
        $sizefile = $_FILES["logo"]["size"];


        $extension = ['jpg','png','gif','jpeg'];
        if(isset($namephoto) AND empty($namephoto)){
            array_push($warnings,"Veuillez séléctionner une image");
        }
        if($sizefile > 15000000){
            array_push($warnings,"Veuillez séléctionner une image de 15MB");
        }
        if(in_array($extension,$typephoto)){
            array_push($warnings,"Ce fichier n'est pas une image");
        }

    }
    if(count($warnings)==0 AND count($errors)==0){
        $namephoto = uniqid().$namephoto;
        move_uploaded_file($filetmp,'assets/logos/'.$namephoto);
        Entreprises::addEntreprise($matriculeentreprise,$nomentreprise,$statusjuridiqueentreprise,$adresseentreprise,$telephone1,$email,$telephone2,$namephoto,$rccm,$_SESSION['bangui']['login'],$idGestionnaire,$identifientutilisateur);
        Utilisateurs::addUsers($identifientutilisateur,hashpassword(PASSWORD),'Administrateur','1',$matriculeentreprise,$email);
        Entreprises::addMatricule();
        array_push($success,"Entreprise enregistrée avec succès");
    }
}

$getGestionnaire = Gestionnaires::getAllGestionnaires();
