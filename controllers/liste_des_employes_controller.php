<?php

$success = [];
$warnings = [];
$errors = [];
use models\Employes;
if(isset($_GET) AND !empty($_GET)){
    extract($_GET);
    if(isset($id) AND !empty($id)){
        try {
            Employes::deleteEmployes($id);
            array_push($success,"Employé supprimer avec succès");
        }catch (PDOException $e){
            array_push($errors,"Erreur de suppression");
        }
    }
}
$getAllEmployes = Employes::getAllEmployes();

